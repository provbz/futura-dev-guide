﻿***************************
Struttura dell'applicazione
***************************

L'applicazione Futura si basa su un Framework sviluppato da Marco Buccio <info@mbuccio.it> per lo sviluppo di applicazioni web basate su PHP.
Tale framework nasce con l'obiettivo di strutturare, uniformare e velocizzare lo sviluppo di applicazioni web completamente customizzate.


Componenti
============

Lo sviluppo applicativo avviene rispettando la seguente struttura:

* action: sono le classi PHP che si occupano di gestire l'interazione con l'utente e la generazione dell'output sia esso costituito da codice HTML o da risposte ad API REST;
* constant: contiene tutte le definizioni trasversali e statiche al sistema;
* css: fogli di stile per la formattazione delle pagine HTML generate;
* env: contiene la configurazione dell'ambiente. si veda la `guida per l'installazione e la configurazione <https://gitlab.com/provbz/futura-tech-guide>`_.
* filter: filtri delle richieste, sono classi utilizzate per gestire aspetti trasversali alle richieste. Ad esempio, il LoginFilter controlla i diritti di accesso di un utente;
* img: risorse statiche;
* lib: librerie utilizzate dall'applicazione;
* script: script JavaScript utilizzati dal frontend;
* service: classi di servizio che implementano logiche di business e accesso ai dati;
* template: template per la produzione di documenti e messaggi email;
* ui: componenti dell'interfaccia grafica;
* utils: classi di utilità;
* Scheduler.php: punto di accesso per i tasks schedulati;
* index.php: punto di accesso per le richieste HTTP sia per le richieste di pagine HTML sia per l'interrogazione di API interne;


Flusso delle richieste
======================

La'plicazione presenta due principali punti di accesso delle richieste


Attività schedulate
-------------------
Queste attività possono essere richiamate solo localmente al sistema direttamente da linea di comando o inserendo 
l'opportuna schedulazione nella crontab. Si veda `la guida di installazione e configurazione <https://gitlab.com/provbz/futura-tech-guide>`_.
Lo scheduler è costituito da una classe che si occupa di rendere disponibili i componenti del framework, attivare la connessione ai 
dati ed invocare l'opportuno metodo, solitamente inserito in una classe di servizio.
La classe scheduler non gestisce la periodicità e la ciclicità dell'invocazione di una chiamata ma solo il rooting applicativo.


Richieste HTTP
--------------
Le richieste HTTP transitano dall'index.php attraverso la configurazione del mod-rewrite di Apache.
L'utilizzo di questo approccio rende uniforme la gestione di tutte le richieste e garantisce un ciclo di richiesta controllato.
I passi principali eseguiti nel processo sono:

* Attivazione dei componenti del framework;
* Connessione al database;
* Parsing della richiesta;
* Istanziazione della classe di riferimento predisposta per la gestione della richiesta;
* attivazione dei filtri di gestione, ad esempio per il controllo degli accessi e per la gestione dei parametri in ingresso;
* Esecuzione del metodo richiesto;
* restituzione dei risultati generati;


Estensioni alla gestione del frontend
=====================================

Nelle ultime iterazioni di implementazione della piattaforma è stato introdotto l'utilizzo del framework per lo sviluppo di SPA: VueJS.
Il framework consente di integrare un approccio di sviluppo del frontend più moderno ad applicazioni web senza la necessità di passare in toto all'approccio previsto per altri sistemi SPA.
In Futura, infatti, VueJS viene usato per la creazione di componenti specifici, ad esempio le tabelle o i test della sezione Mondo delle Parole.
A tendere tutto il frontend dovrebbe essere realizzato con VueJs lasciando lato server la sola fornitura di dati tramite API REST.
Per eseguire la preparazione dei componenti VueJS è necessario eseguire il comando:

```
nom run build
```

Questo genera gli script presenti nella cartella public-html/scripts/mb/vue-components che possono essere rilasciati nei vari ambienti.


Docker
======

Per consentire di attivare rapidamente l'ambiente di sviluppo è stato predisposta la configurazione dell'ambiente tramite docker-compose.
Per avviare l'ambiente in locale è necessario installare Docker desktop ed eseguire il comando:

```
docker-compose up
```


