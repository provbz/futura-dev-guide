﻿******
Entità
******

Di seguito vengono presentate le principali entità di cui è composta l'applicazione:

attachment
	Contiene le informazioni di tutti i files caricati nel sistema

attachment_entity
	Definisce la relazione tra un file caricato e un'entità

banned_ip
	Definisce un elenco di indirizzi ip bloccati a seguito di attività considerate anomale o sospette

dictionary
	Dizionari trasversali all'applicazione;

drop_out_row
	Informazioni legate al modulo di drop out, rappresenta la singola riga di segnalazione

e_model
	Informazioni legate al modello E

e_model_code
	Codici diagnosi inseriti in un modello E

ed_salute
	Informazioni relative ai progetti di Educazione alla Salute

icf
	Contiene la struttura ad albero della tassonomia ICF-CY;
	
icf_metadata
	Associazione dei nodi della tassonomia ICF-CY ai relativi dati descrittivi;
	
icf_modifier
	Modificatori ICF;
	
internal_message
	Contiene i messaggi del sistema di messaggistica interno;

internal_message_module
	Definisce per quali moduli il messaggio risulta visibile;

journal
	Rappresenta un'azione di modifica su una entità;

journal_row
	Dettaglio delle singole modifiche avvenute nell'azione complessiva;

log
	Messaggi di log generati dall'applicazione per tracciare processi e anomalie;
	
mesage
	Messagi inviati agli utenti tramite il sistema di notifiche;

metadata
	Metadati associati alle entità;
	
nazione
	Elenco delle nazioni;

notice
	Notifiche generati da eventi di sistema;
	
notice_user
	Associazione di notifiche agli utenti, data una notifica si puà avere la necessità di informare più utenti dell'evento;
	
notice_user_conf
	Sistema di configurazione delle notifiche, permette di stabilire la relazione di associazione automatica tra utenti e notifiche;

pdp_node
	Contiene la tassonomia visualizzata per i modelli PDP. La tassonomia è stata separata dalla tassonomia PEI

pdp_node_question
	Domande relative ai nodi della tassonomia PDP

pei_node
	Tassonomia PEI;
	
pei_node_question
	Domande di riferimento associate ad un nodo della tassonomia PEI;
	
pei_row
	Associazione di un nodo della tassonomia ad un documento PEI;
	
pei_row_target
	Definizione di obiettivi ed attività associati ad una riga del PEI;
	
property
	Impostazioni tecniche dell'applicazione;
	
role
	Ruoli applicativi
	
role_action
	Associazione dei ruoli ad Action e packages;

scheduler
	Definisce il piano dei task schedulati e il loro stato;

school_year
	Anni scolastici;

somministrazione
	Rappresenta l'insieme di prove da somministrare nel modulo "Mondo delle parole progetto letto scrittura" in un intervallo di date per una specifica classe

somministrazione_test
	I test inseriti in una somministrare

somministrazione_test_result_class
	Le annotazioni relative ad una classe che ha svolto il test di una somministrare

somministrazione_test_result_class_view
	Gli utenti che hanno visualizzato le note inseite nelle restituzioni di classe

somministrazione_test_result_user
	I risultati di un utente per un test

somministrazione_test_result_user_variable
	I risultati di una variabile per un utente in un test;

somministrazione_test_soglia
	Le soglie applicate per un test in una specifica somministrazione;

structure
	Strutture che nell'applicazione rappresentano le scuole;

structure_class
	Le classi presenti in un plesso (struttura);

structure_class_user
	Gli utenti presenti in una classe e il loro ruolo;

taxonomy_node
	Definizione della tassonomia applicata al modello PEI-PDP;

taxonomy_node_metadata_value
	Dati aggiuntivi ai nodi di tassonomia;

taxonomy_node_target
	Rappresenta un obiettivo per un nodo della tassonomia;

taxonomy_node_target_activity
	Rappresenta un'attività per un obiettivo della tassonomia;

test
	Un test caricato nella piattaforma;

test_soglia
	I valori di soglia presenti per un test

user
	Utenti censiti nell'applicazione;
	
user_access
	Storico degli accessi al sistema da parte di ogni utente;

user_attachment
	Allegati legati all'entità utente usati per il portfolio docenti in anno di prova. La gestione dovrebbe passare al sistema centrallizato degli allegati.

user_metadata
	Metadati dell'utente, definiscono aspetti estensivi dell'entità;

user_pcto
	Contiene i dati del PCTO redatto nel PEI;	

user_role
	Ruoli dell'utente;
	
user_structure
	Associazione di un utente ad una struttura/scuola;

user_structure_metadata
	Metadati legati all'entità user_structure;

user_structure_role
	Ruoli rivestiti da un utente in una struttura;
	
user_transfer_request
	Richieste di trasferimento di utenti tra le strutture;
	
user_user
	Associazione di gestione tra utenti. Ad esempio rappresenta i diritti di accesso da parte di un insegnante ai dati di uno studente;
	
user_year
	Dati aggiuntivi applicati ad uno studente;
	
user_year_document
	Documento di uno studente in un anno scolastico;

user_year_document_import
	Importazione di documenti di uno studente in un anno scolastico;