﻿*********************
Sicurezza applicativa
*********************

La sicurezza applicativa nell'acesso alle risorse del frontend è fondamentale per tutte le applicazioni web.
L'applicazione basa la gestione della sicurezza su una serie di meccanismi:

* Gestione centralizzata delle richieste: l'impiego di mod-rewrite consente di centrallizzare e uniformare la procedura di accesso;
* Separazione delle funzionalità in packages: le funzionalità applicative vengono suddivise in packages fortemente vincolati ai permessi dell'utente;
* Strutturazione gerarchica delle Action;
* Accesso gestito ai dati: l'accesso ai dati avviene con query eseguite tramite PDO (PHP Data Object) che permette di limitare i rischi di attacchi;
* L'esposizione di form pubblici viene protetto dalla presenza dell'integrazione con il sistema Google reCAPTCHA per l'individuazione di bot e comportamenti malevoli.
* È stato integrato il login degli utenti tramite Azure AD per consentire un processo di login standardizzato e maggiormente sicuro.

A tali approcci si aggiunge ovviamente una gestione del sistema adeguata e l'accesso all'intero sistema basato esclusivamente su connessioni sicure.


Packages
========

I principali packages di cui è costituita l'applicazione sono:

* admin: contiene tutte le interazioni di tipo amministrativo. I sotto packages sono
	* data: informazioni sui dati;
    * dropOut: informazioni amministrative legate al modulo di Drop Out;
    * edSalute: pannello amministrativo legato ai progetti di Educazione alla Salute;
    * eModel: pannello amministrativo del modulo Modello E;
    * pei: pannello amministrativo modulo PEI-PDP;
    * portfolio: pannello amministrativo legato al modulo Portfolio Docenti;
	* technical: accesso a configurazioni tecniche;
	* user: gestione degli utenti;
* api: accesso alle API di integrazione;
* icf: albero dei contenuti ICF-CY;
* public: tutte le azioni pubbliche tra cui la pagina di login;
* structure: azioni con limite di accesso ai membri scolastici;
    * dropOut: azioni di gestione a livello di struttura del modulo Drop Out;
    * edSalute: azioni di gestione a livello di struttura del modulo di gestione dei progetti di Educazione alla Salute;
    * pdp: gestione PDP a livello di struttura;
    * pei: gestione del PEI a livello di struttura;
    * portfolio: gestione del Portfolio Docenti a livello di struttura;
    * user: gestione dati studente a livello di struttura;
    * pei_tirocinio: gestione del PEI di tirocinio;
    * eModel: ambito del modello E;
    * test: legato al progetto Mondo delle Parole, progetto Letto Scrittura;
    * servizio_valutazione: attiva l'integrazione con il progetto Servizio di Valutazione;
* taxonomy: accesso alla tassonomia del PEI e del PDP;
* user: azioni specifiche dell'utente ad esempio la gesitone del profilo personale;

I ruoli degli utenti sono direttamente legati ad alcuni packages per cui, ad esempio, per un utente con ruolo insegnante sarà impossibile accedere alla sezione admin dato che il ruolo
limita l'accesso al solo package structure.
La definizione dei ruoli è dinamica e risulta modificabile dagli amministratori del sistema che abbiano accesso al package admin/user. 
Si veda `la guida utente per gi amministratori <https://gitlab.com/provbz/futura-user-guide>`_.


Accesso ai dati
===============

Per garantire un approccio logico pulito e consistente l'accesso ai dati viene gestito in modo centrallizzato dall'EntityManager (EM) che fa uso di PDO (PHP Data Object).
È preferibile evitare l'uso dell'EM direttamente dalle classi di action che vengono istanziate in funzione della necessità della richiesta relegando
l'accesso alle classi Service. Ad esempio, ll PeiService conterrà tutte i metodi utilizzati per la gestione del PEI in termini di dati.
Ad esempio il metodo PeiService::create, permette di creare un nuovo PEI per uno specifico utente:

	public static function create($userYearId, $createdByUserId, $type){
        $peiId = EM::insertEntity("user_year_pei", 
                [
                    'user_year_id' => $userYearId,
                    'creation_date' => 'NOW()',
                    'created_by_user_id' => $createdByUserId,
                    'type' => $type
                ]);
        
        return $peiId;
    }
	

Accessi utente
==============

Tutti gli accessi da parte degli utenti vengono salvati in un'apposita tabella e sono visibili dal pannello amministrativo.
Inoltre, dal pannello amministrativo è possibile monitorare gli utenti attualmente loggati.

Il sistema presenta agli utenti che eseguono il login le informazioni relative all'ultimo accesso eseguito (data e ora). 
L'utente può così monitorare eventuali abusi avvenuti sul proprio account richiedendo la chiusura dello stesso.


Monitoraggio dell'attività utente
=================================

Non è presente un monitoraggio persistente delle attività degli utenti ma, per evitare che le sessioni degli utenti rimangano aperte esponendo i dati
a potenziali accessi indesiderati, il sistema prevede un timeout di 10 minuti dopo il quale viene mostrato un messaggio che allerta l'utente dell'imminente
chiusura della sessione nel caso in cui non venga eseguita nessuna azione.


Blocco di IP sospetti
=====================

Nell'applicazione sono implementate alcune regole per bloccare l'accesso da parte di indirizzi IP che eseguano attività sospette.
L'elenco degli IP bloccati è gestibile da pannello amministrativo.
