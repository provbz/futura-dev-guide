﻿futura-dev-guide: Indice
=========================

Il presente manuale descrive la struttura dell'applicazione Futura nei suoi componenti principali, 
il framework di riferimento e le guide linea che risulta opportuno rispettare nell'implementazione di estensioni.

.. toctree::
	:maxdepth: 2
	
	struttura
	sicurezza-applicativa
	entità
	api
