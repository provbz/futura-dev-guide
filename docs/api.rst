***
API
***

Futura implementa un numero limitato di API.
Tali API sono nate per la volontà di consentire a sviluppatori terzi di realizzare moduli esterni in grado di integrarsi con l'applicazione principale.
Le API, come ogni altro endpoint del sistema sono sottoposte alla gestione dei ruoli e dei diritti connessi. Questo significa che è possibile configurare
ruoli che consentano l'accesso solo ad una parte delle API.
Al momento l'unica integrazione presente è con il servizio di valutazione. L'utente associato vede tutti gli endpoint presenti.
Di seguito la descrizione degli endpoints.


Autenticazione
==============

Le API richiedono che le chiamate avvengano da utenti autorizzati.
L'autorizzazione avviene inserendo un'api key ottenuto in fase di creazione dell'utente nel seguente header:

    X-Apikey: [api_key]

Ovviamente, questa modalità di autenticazione è funzionale alle sole integrazioni server to server.
L'api key è da considerare un secret che non deve essere accessibile all'utente o esposto in aplpicazioni SPA o, in generale, nel frontend.


Accesso dell'utente
===================

**URL:** {{url}}/api/user/UserAction

**Metodo:** GET

**Richiesta:** 
    token: String

**Risposta:**
    400: token non specificato

    404: token non valido o utente non online

    200: Ritorna un oggetto contenente le informazioni relative all'utente a cui è associato il token

Questo metodo viene utilizzato per consentire un passaggio trasparente (senza la necessità di eseguire un login separato) dall'applicazione Futura 
al modulo esterno richiesto dall'utente.
Navigando nel sistema l'utente aprirà una pagina conentente un url del tipo:

    https://[modulo_esterno]/?token=[token]

In questo modo il modulo esterno riceve il token da fornire alla chiamata.
Se la chiamata va a buon fine il token è valido, quindi l'applicazione esterna recupera i dati dell'utente:
* dati anagrafici
* associazione agli istituti
* ruoli globali e a livello di istituto

Valutando questi risultati il modulo esterno può decidere se consentire all'utente di proseguire nella navigazione.


Elenco degli uteni
==================

**URL:** {{url}}/api/user/UsersAction

**Metodo:** POST

**Richiesta**::
    
    {
        "first": 0,
        "limit": 10,
        "conditions": {  
            "query": "",
            "meccanografico": "", 
            "role_id": 1
        }
    }
    
first: primo elemento da recuperare.

limit: numero di elementi da recuperare (massimo 100).

conditions: insieme di condizioni/filtri da applicare all'estrazione. Sono attualmente implementati i seguenti filtri:

    query: ricerca generica per nome, cognome o email.

    meccanografico: estrae solo gli utenti presenti in un certo istituto.

    role_id: estra solo gli utenti con un certo ruolo.

**Risposta:**
    400: richiesta non valida

    200: Elenco degli utenti che rispettano i criteri, ad esempio::

        {
            "count": 4,
            "items": [
                {
                    "user_id": 18845,
                    "email": "insegnante.annodiprova@mbuccio.it",
                    "name": "Insegnante",
                    "surname": "Anno di prova"
                },
                ....
            ]
        }

